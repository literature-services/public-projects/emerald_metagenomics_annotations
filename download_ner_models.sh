#!/bin/sh

entities=(engineered ecoregion host place site body-site state treatment sample-material date kit gene primer sequencing LS LCM)
accessions=(MODEL2202170004 MODEL2202170003 MODEL2202170006 MODEL2202170010 MODEL2202170013 MODEL2202160002 MODEL2202170014 MODEL2202170015 MODEL2202170001 MODEL2202170002 MODEL2202170007 MODEL2202170005 MODEL2202170011 MODEL2202170012 MODEL2202170009 MODEL2202170008)

for i in ${!entities[@]};do
echo ${entities[$i]}
echo ${accessions[$i]}
models_dir="entity_classifier/models"
dzip="$models_dir/${entities[$i]}.zip"
if [ -f ${dzip} ]; then rm -rf ${dzip}; fi
if [ -d $models_dir/${entities[$i]} ]; then rm -rf $models_dir/${entities[$i]}; fi
wget -O ${dzip} https://www.ebi.ac.uk/biomodels/model/download/${accessions[$i]}?filename=${entities[$i]}.zip
unzip $dzip -d $models_dir
if [ -f ${dzip} ]; then rm -rf ${dzip}; fi
done
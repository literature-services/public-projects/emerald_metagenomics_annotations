## **EMERALD METAGENOMICS ANNOTATIONS** 

- This repository is a slightly simplified version of the current metagenomics annotations pipeline used by Europe PMC to regularly annotate open access publications with 16 novel metagenomics entities. 

- This work was developed for the  [EMERALD](https://gtr.ukri.org/projects?ref=BB%2FS009043%2F1) project to enrich metagenomics studies in [MGnify](https://www.ebi.ac.uk/metagenomics/) with metadata from research articles in [Europe PMC](http://europepmc.org/search?query=%28ANNOTATION_PROVIDER%3A%22Metagenomics%22%29&page=1). For more information, please refer to the following blogs in [Europe PMC](http://blog.europepmc.org/2020/11/europe-pmc-publications-metagenomics-annotations.html) and [MGnify](https://www.ebi.ac.uk/about/news/service-news/enriched-metadata-fields-mgnify-based-text-mining-associated-publications)

#### **System Requirements**

- This repository requires a computer with `RAM: 16GB`, `CPU: 6vCore`, `SSD: 30GB`. This version has been tested on Linux operating system and [Google Colab](https://colab.research.google.com) .

#### **Installation**
  
  - Clone emerald_metagenomics_annotations repository:
  
    ```
    $ git clone https://gitlab.ebi.ac.uk/literature-services/public-projects/emerald_metagenomics_annotations
    $ cd emerald_metagenomics_annotations
    ```
    ##### _*If you are using Google Colab, you need to run `%cd emerald_metagenomics_annotations` to change your current Colab directory to `emerald_metagenomics_annotations`._    
     
  - Install miniconda, if you don't have anaconda or python already installed:
  
    ```
    $ wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.11.0-Linux-x86_64.sh    
    $ bash Miniconda3-py39_4.11.0-Linux-x86_64.sh
    $ source ~/.bashrc
    ```
    
    ##### _*On executing `bash Miniconda3-latest-Linux-x86_64.sh`, you need to type yes when requried. `source ~/.bashrc` will be required to add conda aliases and paths to your shell environment. If you are using Google Colab, you can skip installing miniconda._ 
            
  - Create metagene conda environment:
  
    ```
    $ conda create -n metagene python=3.7
    ```
    ##### _*If you are using Google Colab, you can skip creating metagene environment._
     
  - Activate metagene environment and install python packages:
  
    ```
    $ conda activate metagene
    $ pip install -r requirements.txt
    ``` 
    ##### _*If you are using Google Colab, you can skip activating metagene environment and install the requirements directly._
    
  - Download and unzip NER models directly into `entity_classifier/models` by running the following command (This step takes ~15-30 min):
  
    ```
    $ bash download_ner_models.sh
    ```
    
    ##### _*You can also download and upload them manually from BioModels - see the table below._
    

#### **Classifying Metagenomics Literature**

  - You can classify metagenomics literature into `Engineered`, `Host-associated` or `Environmental` microbiome environments using the random forest (rf) classifiers in `text_classifier/models` folder. 
  
  - Two models were trained on the publications listed in `text_classifier/input/rf_training_pmcs.csv`:
  
      - doc2vec random forest classifer `text_classifier/models/random_forest/rf_9_250_25_k5_n100_t0.2_d2_doc2vec_all_model.pkl` was trained on publications Doc2Vec generated from doc2vec model `text_classifier/models/doc2vec/mgnify_doc2vec_200_5.model`. 
      
      - tfidf random forest classifer `text_classifier/models/random_forest/rf_9_300_25_k5_n100_t0.2_d2_tfidf_all_model.pkl` was trained on publications TF-IDF. 
  
  - To classify texts, run the following command with the following arguments:
  
      ```
      $ python scripts/classify_texts.py
          <path to your texts directory>
          <random forest model path> - tfidf or doc2vec random forest classifer path  
          <feature_type> - tfidf or doc2vec
          <doc2vec model path> - None if tfidf random forest classifer is selected and doc2vec model path if doc2vec is selected
          <prediction probability threshold>  - 0.4 or 0.5
          <return predicted text in the predictions files> - yes or no 
          <path to output directoy>
      ```
  
  - For example, run the following command to classify texts examples in `texts` using tfidf random forest classifier and predictions will be saved in csv files in `text_classifier/output` :
  
      ```
      $ python scripts/classify_texts.py texts text_classifier/models/random_forest/rf_9_300_25_k5_n100_t0.2_d2_tfidf_all_model.pkl tfidf None 0.4 no text_classifier/output
      ```

  - For example, run the following command to classify texts examples in `texts` using Doc2Vec random forest classifier and predictions will be saved in csv files in `text_classifier/output` ::

      ```
      $ python scripts/classify_texts.py texts text_classifier/models/random_forest/rf_9_250_25_k5_n100_t0.2_d2_doc2vec_all_model.pkl doc2vec text_classifier/models/doc2vec/mgnify_doc2vec_200_5.model 0.4 no text_classifier/output
      ```   

#### **Annotating Literature with Metagenomics Entities**

  - You can annotate metagenomics literature with 16 novel metagenomics entities using the metagenomics Named-Entity-Recognition (NER) models you downloaded into `entity_classifier/models` folder.
  
  - 16 metagenomics NER models were trained on the curated datasets in `entity_classifier/input/dataset.csv` to identify and annotate metadata related to the following entities:
  
      | Entity | Definition | BioModels Download URLs |
      | ---      | ---      | ---      | 
      | 1. _**Engineered**_   | Microbiome man-made environment   | [MODEL2202170004](https://www.ebi.ac.uk/biomodels/MODEL2202170004)   |
      | 2. _**Ecoregion**_   | Microbiome natural environment   | [MODEL2202170003](https://www.ebi.ac.uk/biomodels/MODEL2202170003)   |
      | 3. _**Host**_   | Microbiome living organism or host   | [MODEL2202170006](https://www.ebi.ac.uk/biomodels/MODEL2202170006)   |
      | 4. _**Place**_   | Microbiome geographical place   | [MODEL2202170010](https://www.ebi.ac.uk/biomodels/MODEL2202170010)   |
      | 5. _**Site**_   | Microbiome local environment or site   | [MODEL2202170013](https://www.ebi.ac.uk/biomodels/MODEL2202170013)   |
      | 6. _**Body-Site**_   | Microbiome body site   | [MODEL2202160002](https://www.ebi.ac.uk/biomodels/MODEL2202160002)   |
      | 7. _**State**_   | The state of microbiome environment or host   | [MODEL2202170014](https://www.ebi.ac.uk/biomodels/MODEL2202170014)   |
      | 8. _**Treatment**_   | The treatment of microbiome environment or host   | [MODEL2202170015](https://www.ebi.ac.uk/biomodels/MODEL2202170015)   |
      | 9. _**Sample-Material**_   | The material of microbiome sample   | [MODEL2202170001](https://www.ebi.ac.uk/biomodels/MODEL2202170001)   |
      | 10. _**Date**_   | Microbiome collection date   | [MODEL2202170002](https://www.ebi.ac.uk/biomodels/MODEL2202170002)   |
      | 11. _**Kit**_   | Microbiome nucleic acid extraction kit   | [MODEL2202170007](https://www.ebi.ac.uk/biomodels/MODEL2202170007)   |
      | 12. _**Gene**_   | Microbiome target gene   | [MODEL2202170005](https://www.ebi.ac.uk/biomodels/MODEL2202170005)   |
      | 13. _**Primer**_   | PCR primer   | [MODEL2202170011](https://www.ebi.ac.uk/biomodels/MODEL2202170011)   |
      | 14. _**Sequencing**_   | Sequencing platform or method   | [MODEL2202170012](https://www.ebi.ac.uk/biomodels/MODEL2202170012)   |
      | 15. _**LS**_  | DNA Library strategy   | [MODEL2202170009](https://www.ebi.ac.uk/biomodels/MODEL2202170009)   |
      | 16. _**LCM**_   | DNA library construction method   | [MODEL2202170008](https://www.ebi.ac.uk/biomodels/MODEL2202170008)   |

  
  - **To annotate texts, you need to**:
      
      1. Preprocess and tokenize your texts (e.g. `texts` folder) for the 16 metagenomics NER models:
      
          ```
          $ python scripts/run_ner_process.py entity_classifier bio-text texts 
          ```
          
          ##### _*The `texts` argument can be changed to the path to your texts folder. This command will generate tokenized sentences in each model folder in `entity_classifier/input` folder._
          
          
      2. Annotate preprocessed texts with each of the 16 metagenomics NER models (10 texts examples can take from ~20-60 min on CPU and ~8-10 min on [Google Colab](https://colab.research.google.com) GPU):
      
          ```
          $ bash scripts/run_ner.sh entity_classifier python
          ``` 
          
          ##### _*The `python` argument can be changed to your conda environment python path, e.g., `<path to your miniconda>/metagene/python`._
          
      3. Postprocess and normalize metagenomics annotations to ontologies:
      
          ```
          $ python scripts/run_ner_process.py entity_classifier process-ner texts entity_classifier/output 
          ``` 
          
          ##### _*The `texts` and `entity_classifier/output` can be changed to the path to your texts and output folders, respectively. This command will generate your final normalized annotations in json files for download in `entity_classifier/output/sent-json` (Europe PMC sentence annotations format) and `entity_classifier/output/ent-json` (Europe PMC entity annotations format)._           


#### **Citation** 

  - For usage of metagenomics annotations repository, please cite the following preprint:
  
    Nassar, M., Rogers, A., Talo’, F., Sanchez, S., Finn, R., & Mcentyre, J. (2022). **A machine learning framework for discovery and enrichment of metagenomics metadata from open access publications.** https://doi.org/10.21203/RS.3.RS-1396476/V1  
  

#### **Contact information** 

  - For help or issues using metagenomics annotations repository, please contact Maaly Nassar (maaly13@yahoo.com). 

#### **License information** 
  - CC-by

"""
### EMEARALD PIPELINE ###
ner_process class comprises all the functions needed to preprocess texts for metagenomics NER models and postprocess metagenomics annotations
# @author: Maaly Nassar
# @email:maaly13@yahoo.com
"""

import spacy
nlp = spacy.load("en_core_sci_lg")
nlp.max_length = 50000000000
import re
import os
import pathlib
from pathlib import Path
import string
import csv
import random
import json
from urllib.parse import urlparse
import shutil 
import pandas as pd
from pathlib import Path
import numpy as np
from ast import literal_eval
import requests
import pandas as pd
import nltk
nltk.download('stopwords')


class ner_process:
    def __init__(self):
        np.random.seed(1330)
        pass 

    def bert_tokenize(self,text,tokenizer):
        #print(text)      
        tokens = []
        for t in tokenizer.tokenize(text):
            #print(t)
            if t[:2] == '##': # if it is a piece of a word (broken by Word Piece tokenizer)
                tokens[-1] = tokens[-1]+t[2:] # append pieces
            else:
                tokens.append(t)
        #print(tokens)
        return tokens

    def sentencise_predict_texts(self,txt_dir,bert_tokenizer):        
        
        ### this function tokenizes texts sentences using spacy and BERT tokenizer ###
        
        files = Path(txt_dir).glob('**/*.txt')        
        all_sents = {}
        all_sents['PMC'] = []
        all_sents['SENT'] = [] 
        owords = [] # o tagged words
        
        for i, f in enumerate(files):
            pmc = re.sub(r'.txt','',f.parts[-1])
            text = f.read_text()        
            if text != '':
                doc = nlp(text)    
                sents = [sent.text for sent in doc.sents]
                for sent in sents:
                    sent = str(sent).strip()         
                    words = self.bert_tokenize(sent,bert_tokenizer) #bert-tokenize
                    if words!=[]:    
                        all_sents['PMC'].append(pmc)
                        all_sents['SENT'].append(sent)
                        for w in words:
                            owords.append(w+'\t'+'O')
                        owords.append('')
                    #else:
                        #print(words)
                        #print(encoded_words)
            print(pmc+' is tokenized for BERT NER task')        
        
        return [all_sents,owords]    

    def bio_tag_predict(self,txt_dir,entities,pred_input_dir,bert_tokenizer):
                
        ### tag the datasets to be annotated with NER models with O tags ###
        [all_sents,owords] = self.sentencise_predict_texts(txt_dir,bert_tokenizer)
        
        ### save tokenized datasets as test.tsv ###                    
        for entity in entities:
            if Path(pred_input_dir,entity,'test.tsv').exists(): os.remove(Path(pred_input_dir,entity,'test.tsv'))
            predf = open(Path(pred_input_dir,entity,'test.tsv'), 'a')
            for ow in owords:
                predf.write(ow+'\n')
            predf.close()

    def conll_to_csv(self,entity,conll_path):
        
        ### this function reads NER_result_conll.txt for each model and generates a csv file per each model ###
        
        pred = {'SENT-TOKENS':[],'TERM':[],'TERM-POS':[],'BIO':[],'URL':[]} # csv columns
        tks = []
        ptks = []
        plbls = []
        urls = []    
        marktrm = ''
        marklbl = ''   
        lbl = 'O-MISC'
        tkpos = -1
        pos = []
        with open(conll_path,'r') as pred_tok:
            for line in pred_tok:
                if line == '\n':
                    #if ptks != []:
                    pred['SENT-TOKENS'].append(tks)
                    pred['TERM'].append(ptks)
                    pred['TERM-POS'].append(pos)                
                    pred['BIO'].append(plbls)
                    pred['URL'].append(urls)                
                    tks = []
                    ptks = []
                    plbls = []
                    urls = []
                    marktrm = ''
                    marklbl = ''
                    lbl = 'O-MISC'                
                    tkpos = -1
                    pos = []
                    continue
                else:
                    tkpos +=1
                    sline = line.split()                
                    #print(sline)
                    tk = str(sline[0])
                    olbl = sline[1] # original label
                    plbl = sline[2] # predicted label
                    tks.append(tk)                   
                    if plbl in ['B-MISC','I-MISC']:                        
                        if lbl == 'O-MISC':
                            marktrm=tk
                            marklbl=plbl 
                        elif lbl in ['B-MISC','I-MISC']:
                            marktrm=marktrm+' '+tk 
                            marklbl=marklbl+' '+plbl                                                 
                    elif plbl == 'O-MISC':
                        if lbl in ['B-MISC','I-MISC']:                        
                            ptks.append(marktrm)
                            pos.append(tkpos)
                            plbls.append(marklbl)
                            url = self.normalize_tag(marktrm,entity)
                            urls.append(url)                                     
                    lbl = plbl
        pred_df = pd.DataFrame(pred)
        return pred_df
        
    def normalize_tag(self,trm,entity):
        
        ### this function normalizes annotations to OLS ontologies ###
        
        #print(trm)
        ourl = 'https://www.ebi.ac.uk/ols/search?q='+trm
        try:
            if entity not in ['primer','kit']:
                url = 'https://www.ebi.ac.uk/spot/zooma/v2/api/services/annotate?propertyValue='+trm         
                response = requests.get(url)    
                rjson = response.json()
                if rjson != []:
                    rslt = rjson[0]
                    lnk = rslt['_links']['olslinks'][0]
                    gurl = lnk['semanticTag'] if 'semanticTag' in lnk.keys() else []
                    #print(gurl)
                    if 'http://purl.obolibrary.org/obo/' in gurl:
                        db_id = urlparse(gurl).path.split('/')[2:3][0]            
                        db = db_id.split('_')[0]                       
                        #print('semantic_tag: '+gurl)
                        ourl = 'https://www.ebi.ac.uk/ols/ontologies/'+db+'/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2F'+db_id           
                    else:
                        ourl = gurl
            elif entity == 'kit':
                trm = trm.replace(' ','-')
                #ourl = 'https://www.biocompare.com/Assay-Kit-Product-Search/?search='+trm 
                ourl = 'https://www.biocompare.com/General-Search/?search='+trm

            elif entity == 'primer':
                csvf = Path('scripts/bert/primer_probebase.csv')
                if csvf.exists(): 
                    prmdf = pd.read_csv(csvf)
                    for p,prm in enumerate(prmdf['NAME']):
                        if (prm in trm) or (trm == prm):
                            ourl = list(prmdf['URL'])[p]
                            break;
        except:
            #print(trm+':error')
            pass
        #print(ourl)
        return ourl

    def epmc_sentence_anns(self,catgs,pdfsg,predict_dir,json_dir):
        
        ### this function postprocsss/cleans NER results and generates Europe PMC sentence annotations in jsons ###
        
        xtrms = ['whole','abbr.','abbr','user','users','SI','`-end','-end','total','slide','slides','did not'] #blacklist terms
        symbs = [")","(","≥","≤",">","<","~","/",".","-","%","=","+",":","%","°C","ºC","–","±","_","[","]","″","′","’","'","‐"]
        symbs2 = ["≤","≥",">","<","~","/",".","-","=","±",":","–","_","″","′","’","'","‐"]
        stpwrds = nltk.corpus.stopwords.words('english')
        stpwrds2 = [*nltk.corpus.stopwords.words('english'),*[word.title() for word in nltk.corpus.stopwords.words('english')]]
        xannpmcs = []
        
        for p,pmcid in enumerate(pdfsg['PMC']):
            #print(pmcid)
            mtgn = {} # epmc annotation schema
            mtgn['src'] = 'PMC'
            mtgn['id'] = pmcid      
            mtgn['provider'] = 'emerald'    
            mtgn['anns'] = []
            for catg in catgs:
                for s,snt in enumerate(pdfsg['SENT'][p]):
                    stks = pdfsg['SENT-TOKENS'][p][s] # sentence tokens
                    strms = pdfsg[catg+'_TERM'][p][s] # sentence annotated terms
                    spos = pdfsg[catg+'_TERM-POS'][p][s] # sentence annotated terms numbers
                    sbios = pdfsg[catg+'_BIO'][p][s] # sentence annotated terms bio 
                    surls = pdfsg[catg+'_URL'][p][s] # sentence annotated terms url
                    strms2 = [] # sentence annotated terms corrected 
                    surls2 = [] # sentence urls corrected
                    prfxs = []
                    sfxs = []
                    if strms !=[]:
                        sanns = {} # sentence epmc annotations schema
                        sanns['exact'] = snt
                        sanns['section'] = 'Methods (http://purl.org/orb/Methods)'
                        sanns['type'] = catg
                        sanns['tags']=[]
                        for st,strm in enumerate(strms):                    
                            tbio = sbios[st].split() #term bio                      
                            turl = surls[st] # term url                                        
                            tpos = spos[st] # term number                   
                            ttks = [] # term tokens
                            ttks = strm.split(' ') # term tokens                       
                            if len(ttks)>1:
                                tpos2 = [tpos-tk for tk,ttk in enumerate(ttks)]
                                tpos2.sort()
                            else:
                                tpos2 = [tpos] # term corrected numbers
                            ttks2 = [stks[ps-1] for ps in tpos2] # term tokens corrected

                            ### getting BERT missed B-MISC terms ###
                            ########################################
                            try:
                                if (tbio[0] == 'I-MISC') or (ttks2[0] in symbs2):
                                    if tpos2[0]>1: 
                                        tpos2 = [*[tpos2[0]-1],*tpos2]                                
                                        ttks2 = [stks[ps-1] for ps in tpos2]
                                        ttks3 = ttks2
                                        if tpos2[0]>1:ttks3 = [stks[ps-1] for ps in [*[tpos2[0]-1],*tpos2]]
                                        while (ttks2[0] in symbs or ttks2[0] in stpwrds or ttks3[0] in symbs) and tpos2[0]>1:
                                            tpos2 = [*[tpos2[0]-1],*tpos2]                                
                                            ttks2 = [stks[ps-1] for ps in tpos2]
                                            ttks3 = ttks2
                                            if tpos2[0]>1:ttks3 = [stks[ps-1] for ps in [*[tpos2[0]-1],*tpos2]]                                                

                                if tpos2[-1]+1<len(stks):ttks3 = [stks[ps-1] for ps in [*tpos2,*[tpos2[-1]+1]]]
                                if tpos2[-1]<len(stks) and (ttks2[-1] in symbs2 or ttks2[-1] in stpwrds or ttks3[-1] in symbs2):
                                    tpos2 = [*tpos2,*[tpos2[-1]+1]]
                                    ttks2 = [stks[ps-1] for ps in tpos2]
                                    ttks3 = ttks2
                                    if tpos2[-1]+1<len(stks):ttks3 = [stks[ps-1] for ps in [*tpos2,*[tpos2[-1]+1]]]                               
                                    while tpos2[-1]<len(stks) and ttks2[-1] in symbs2:                                
                                        tpos2 = [*tpos2,*[tpos2[-1]+1]]
                                        ttks2 = [stks[ps-1] for ps in tpos2]
                                        ttks3 = ttks2
                                        if tpos2[-1]+1<len(stks):ttks3 = [stks[ps-1] for ps in [*tpos2,*[tpos2[-1]+1]]]

                                while ttks2[0] in [',',')',']',';'] or (ttks2[0] in stpwrds2 and ttks[0] not in stpwrds2) or (ttks2[0] in symbs and ttks[0] not in symbs): ttks2 = ttks2[1:]
                                while ttks2[-1] in [',','(','[',';'] or (ttks2[-1] in stpwrds2 and ttks[-1] not in stpwrds2) or (ttks2[-1] in symbs and ttks[-1] not in symbs): ttks2 = ttks2[:-1]

                            except:                        
                                #print('##########')
                                #print(snt)
                                #print(strm)
                                #print(ttks2)                        
                                #print('##########')
                                pass

                            ### getting BERT missed NER tokens from original sentences ###
                            ##############################################################
                            trms = []
                            rgxs = ['\s?','.{0,1}','.{0,2}','.{0,3}','.{0,4}','.{0,5}','.{0,6}','.{0,7}'] # regular expressions set                        
                            for rgx in rgxs:                            
                                strm2 = ' '.join(ttks2)
                                strm2 = re.escape(strm2)                                
                                strm2 = strm2.replace('\ ',rgx)                                 
                                trms = re.findall(strm2,snt)
                                trmspos = [p.start() for p in re.finditer(strm2,snt)]                           
                                if trms != []:                            
                                    break
                                #else:
                                    #print('#####')
                                    #print(snt)
                                    #print(strm2)
                                    #print(rgx)
                                    #print(trms)
                                    #print('#####')

                            # processing primer tokenizing error #
                            if catg == 'primer' and trms == []:                    
                                for rgx in rgxs:                            
                                    strm2 = ' '.join(ttks2)                            
                                    if not re.search(r'.+[5\′]',strm2) is None:
                                        res = re.search(r'.+[5\′]|',strm2)
                                        strm2 = res.group()
                                        strm2 = strm2.replace('5 ′','')
                                        strm2 = re.escape(strm2)                                
                                        strm2 = strm2.replace('\ ',rgx)                                 
                                        trms = re.findall(strm2,snt)
                                        trmspos = [p.start() for p in re.finditer(strm2,snt)]                           
                                        if trms != []:                            
                                            break
                                        #else:
                                            #print('#####')
                                            #print(snt)
                                            #print(strm2)
                                            #print(rgx)
                                            #print(trms)
                                            #print('#####')                 


                            # remove common false positives terms #                    
                            if catg not in ['date','place','primer','gene']:
                                trms = [trm for trm in trms if re.search(r'^[0-9\%\≤\≥\>\<\~\/\.\-\=\±\:\−\–\_\″\′\’\'\"\]\[\)\(\‐\$\,\&\#\*\+\?\@\`\{\}\~]+$',trm) is None]
                            if catg in ['place','site']:
                                trms = [trm for trm in trms if re.search(r'^[A-Z]{1}\.\s?[a-z]+$',trm.strip()) is None]    
                            trms = [trm for trm in trms if trm.lower() not in xtrms and re.search(r'^[0-9\.\,]{0,7}\s?[a-zμ]{1,2}\/[a-zμ]{1,2}$',trm.lower().strip()) is None]
                            # getting term context #
                            #print(rgx)
                            cpos = tpos2

                            if len(list(np.unique(trms)))==1:
                                ctxt = re.search(strm2,snt)
                                cpos = tpos2
                            else:                                           
                                if tpos2[0]-2>0 and tpos2[-1]+2<=len(stks):
                                    cpos = [*[tpos2[0]-2],*[tpos2[0]-1],*tpos2,*[tpos2[-1]+1],*[tpos2[-1]+2]]       
                                elif tpos2[0]-1>0 and tpos2[-1]+1<=len(stks):
                                    cpos = [*[tpos2[0]-1],*tpos2,*[tpos2[-1]+1]]
                                elif tpos2[0]==1 and tpos2[-1]+2<=len(stks):
                                    cpos = [*tpos2,*[tpos2[-1]+1],*[tpos2[-1]+2]]
                                elif tpos2[0]==1 and tpos2[-1]+1<=len(stks):
                                    cpos = [*tpos2,*[tpos2[-1]+1]]
                                elif tpos2[-1]==len(stks) and tpos2[0]-1>0:
                                    cpos = [*[tpos2[0]-1],*tpos2]        
                                elif tpos2[-1]==len(stks) and tpos2[0]-2>0:
                                    cpos = [*[tpos2[0]-2],*[tpos2[0]-1],*tpos2]                        

                            for rgx in rgxs:
                                ctxt = ' '.join([stks[ps-1] for ps in cpos])
                                ctxt = re.escape(ctxt)
                                ctxt = ctxt.replace('\ ',rgx)               
                                ctxt = re.search(ctxt,snt)
                                if not ctxt is None:
                                    break;

                            if ctxt is not None:
                                cpos = ctxt.span()                                
                            #else:
                                #print('#######')
                                #print(stks)
                                #print(strm)
                                #print(tpos2)
                                #print(ttks2)
                                #print(strm2)                 
                                #print('#######')                                

                            # adding terms #                       
                            if trms!=[]:                               
                                for t,trm in enumerate(trms):                         
                                    if trmspos[t] in range(cpos[0],cpos[1]) and trm not in [*strms2,*symbs,*stpwrds]:
                                        strms2.append(trm)                                    
                                        surls2.append(turl)
                                        prfxs.append(snt[0:trmspos[t]])
                                        sfxs.append(snt[trmspos[t]+len(trm):])


                    ### adding and normalizing processed annotations ###
                    ####################################################
                    if strms2 !=[]:                   
                        #if len(np.unique(strms2))!=len(np.unique(strms)):
                            #print(snt)
                            #print(stks)
                            #print(sbios)
                            #print(spos)
                            #print(strms)
                            #print(strms2)                       
                        for t,trm in enumerate(strms2):
                            trmo={}
                            trmo['name']=trm
                            trmo['prefix']=prfxs[t]
                            trmo['postfix']=sfxs[t]
                            if trm not in strms:
                                tagurl2 = self.normalize_tag(trm,catg)
                                trmo['uri']=tagurl2 if tagurl2!=''else 'https://www.ebi.ac.uk/ols/search?q='+trm
                            else:
                                trmo['uri']=surls2[t] if surls2[t]!='' else 'https://www.ebi.ac.uk/ols/search?q='+trm
                            sanns['tags'].append(trmo)               
                        mtgn['anns'].append(sanns)

            ### saving annotations ###
            ##########################
            if mtgn['anns']!=[]:        
                ### saving annotations to publication json ###        
                with open(Path(predict_dir,json_dir,pmcid+'.json'), 'w', encoding='utf-8') as j:
                    json.dump(mtgn,j,ensure_ascii=False,indent=4)
            else:
                xannpmcs.append(pmcid)

        
    def epmc_entity_anns(self,jsfs,odir):
        
        ### this function generates EuropePMC entity annotations in json from EuropePMC sentence annotations ###
        
        splt = 9999 # number of json per epmc annotation file
        range1 = list(range(0,len(jsfs),splt)) 
        range2 = [*list(range(splt,len(jsfs),splt)),*[len(jsfs)]]  
        fsplts = []
        for s,strt in enumerate(range1):
            fsplt = jsfs[strt:range2[s]]
            fsplts.append(fsplt)
            #print(len(fsplt))        
        
        for f,fsplt in enumerate(fsplts):    
            n_splt = f
            xpfxs = []    
            for j,jsf in enumerate(fsplt):
                jtyps = []
                jf = jsf.parts[-1] 
                if jf != 'emerald_anns.json':
                    #print(j)
                    #print(jf)
                    with open(jsf) as ijf:
                        js = json.load(ijf)           
                    ejs = {}
                    ejs['src'] = js['src']
                    ejs['id'] = js['id']
                    ejs['provider'] = 'Metagenomics' 
                    ejs['anns'] = []           
                    for ann in js['anns']:
                        xfxs = []
                        snt = ann['exact']
                        typ = ann['type']
                        anntags = [tag['name'] for tag in ann['tags']]
                        for tag in ann['tags']:                    
                            if (tag['prefix'].strip() not in xfxs) or (tag['postfix'].strip() not in xfxs):
                                ent = {}
                                tag2 = ''
                                for anntag in anntags:
                                    if tag['name']+' '+anntag in snt:
                                        tag2 = tag['name']+' '+anntag 
                                        ent['exact'] = tag2
                                        tagpos = re.search(re.escape(tag2),snt).span() 
                                        ent['prefix'] = snt[:tagpos[0]]
                                        ent['postfix'] = snt[tagpos[1]:]
                                        xfxs = [*xfxs,*snt[:tagpos[0]].strip(),*snt[tagpos[1]:].strip()]
                                        tag2url = self.normalize_tag(tag2,typ)
                                        tag2url = tag2url if tag2url!='' else 'https://www.ebi.ac.uk/ols/search?q='+tag2
                                        ent['tags'] = [{'name':tag2,'uri':tag2url}]                               
                                        break

                                if tag2 =='':
                                    ent['exact'] = tag['name']
                                    ent['prefix'] = tag['prefix']
                                    ent['postfix'] = tag['postfix']
                                    ent['tags'] = [{'name':tag['name'],'uri':tag['uri']}]

                                ent['position'] = '1.0'                   
                                ent['section'] = 'Methods (http://purl.org/orb/Methods)'
                                ent['type'] = typ # lower case - types should be lower case
                                jtyps.append(typ)
                                #print(ent)
                                if (tag['prefix'] in ['',None] and tag['postfix'] in ['',None]) or (tag['name']==''): 
                                    xpfxs.append([js['id'],tag])                    
                                else:
                                    ejs['anns'].append(ent)                    
                    #print(ejs)
                    with open(Path(odir,jf), 'w', encoding='utf-8') as ojf:
                            json.dump(ejs,ojf,ensure_ascii=False,indent=4) #indent format the json


    def process_biobert_predict_dataset(self,txt_dir,bert_tokenizer,models_dir,output_dir):

            ### sentencize texts ###
            [all_sents,owords] = self.sentencise_predict_texts(txt_dir,bert_tokenizer)
            sentsdf = pd.DataFrame(all_sents)
            sentsdfg = sentsdf.groupby(['PMC'],sort=False)['SENT'].apply(list).reset_index()
            print(sentsdfg)
            
            ### process conll entity predictions and save processed entities to csv files ###
            pdfs = {} # predicted entities data frames
            hdf = pd.read_csv(Path(models_dir,'ner-grid-metrics.csv')) # high models metrics csv
            for r,row in hdf.iterrows():
                lr = '3e-05' if row['learning_rate'] == 2.9999999999999997e-05 else str(row['learning_rate'])
                mdl = lr+'-'+str(row['epoch']) # predicted entites directory
                csvdir = Path(output_dir,'model-'+mdl)
                if not os.path.isdir(csvdir):os.mkdir(csvdir,0o775)
                pred_token_path = Path(models_dir,row['category'],mdl,"NER_result_conll.txt")
                if pathlib.Path(pred_token_path).exists():
                    pdf = self.conll_to_csv(row['category'],pred_token_path)
                    pdf.to_csv(Path(csvdir,"NER_result_"+row['category']+".csv"),encoding='utf-8',index=False)
                    os.chmod(Path(csvdir,"NER_result_"+row['category']+".csv"), 0o775)      
                    #converters = {}
                    #for col in ["TERM","TERM-POS","BIO","URL","SENT-TOKENS"]:converters[col] = literal_eval
                    #pdf = pd.read_csv(Path(output_dir,mdl,'NER_result_'+row['category']+'.csv'),converters=converters)
                    pdfs['SENT-TOKENS'] = pdf['SENT-TOKENS'] if 'SENT-TOKENS'not in pdfs.keys() else pdfs['SENT-TOKENS']
                    pdfs[row['category']+'_TERM'] = pdf['TERM']
                    pdfs[row['category']+'_TERM-POS'] = pdf['TERM-POS']
                    pdfs[row['category']+'_BIO'] = pdf['BIO']
                    pdfs[row['category']+'_URL'] = pdf['URL']
                    #print(mdl)
            pdfs = pd.DataFrame(pdfs) 
            pdfs['SENT'] = sentsdf['SENT']
            pdfs['PMC'] = sentsdf['PMC']
            #print(pdfs[['SENT','SENT-TOKENS']])
            
            ### aggregate annotated entities ### 
            agg = {'SENT':list,'SENT-TOKENS':list}
            for catg in hdf['category']:
                for col in ['TERM','TERM-POS','BIO','URL']:
                    agg[catg+'_'+col] = list
            pdfsg = pdfs.groupby(['PMC'],sort=False).agg(agg).reset_index()           
            
            ### process annotations in EPMC sentence annotations format ###
            json_dir = 'sent-json'
            if os.path.isdir(Path(output_dir,json_dir)):
                shutil.rmtree(Path(output_dir,json_dir))
            os.mkdir(Path(output_dir,json_dir),0o775)
            self.epmc_sentence_anns(list(hdf['category']),pdfsg,output_dir,json_dir)
            print('EPMC sentence annotations were successfully completed')
            
            ### process annotations in EPMC entity annotations format ###           
            jsfs = list(Path(output_dir,json_dir).glob('**/*.json'))
            odir = Path(output_dir,'ent-json')
            if os.path.isdir(odir):
                shutil.rmtree(odir)
            os.mkdir(odir,0o775)
            self.epmc_entity_anns(jsfs,odir)
            print('EPMC entity annotations were successfully completed')
            
            

        
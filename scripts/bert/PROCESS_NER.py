import nltk #pip install -U nltk
from nltk.tokenize import wordpunct_tokenize
from nltk.tokenize import sent_tokenize, word_tokenize
import spacy
nlp = spacy.load("en_core_sci_lg")
#nlp = spacy.load("en_core_web_lg")
nlp.max_length = 2000000
import re
import os
import string
import csv
import random
import json
import pathlib
from urllib.parse import urlparse
import shutil 
import pandas as pd
from pathlib import Path
import numpy as np
from ast import literal_eval
import requests
import pandas as pd
from sklearn.metrics import precision_recall_fscore_support as score #pip install -U scikit-learn
from sklearn.model_selection import train_test_split
from scipy.spatial import distance
from gensim.models import FastText
from gensim.models.keyedvectors import KeyedVectors
from collections import Counter
from glob import glob

### PATHS FOR ENA ###
#####################
# db='ena'
# n_splt = 0
# txt_dir='/nfs/production/literature/maaly/emerald-master/ft/ena/txt'
# predict_dir = '/nfs/production/literature/maaly/emerald-master/ml/ner/biobert/predict/ena/models/tensorflow/ena_all_9010_ena'
# db_sect = 'method'


### PATHS FOR ENA ###
#####################
db='ena'
n_splt = 0
txt_dir='/nfs/production/literature/maaly/emerald-master/ft/ena/txt'
#predict_dir = '/nfs/production/literature/maaly/emerald-master/ml/ner/biobert/predict/ena/models/tensorflow/ena_all_9010_ena_d2v'
#db_sect = 'method_d2v'
predict_dir = '/nfs/production/literature/maaly/emerald-master/ml/ner/biobert/predict/ena/models/tensorflow/ena_all_9010_ena_tfidf'
db_sect = 'method_tfidf'


### PATHS FOR METAGENE ###
##########################
# db='metagene'
# with open(Path('/nfs/production/literature/maaly/emerald-master','xrf',db,db+'_oapmcids.json')) as json_file: oapmcs = json.load(json_file)
# splt = 9999
# start = list(range(0,len(oapmcs),splt))
# end = [*list(range(splt,len(oapmcs),splt)),*[len(oapmcs)]]

# ### PATHS FOR METAGENE ###
# txt_dirs = []
# predict_dirs = []
# for n,s in enumerate(start):
#     txt_dir='/nfs/production/literature/maaly/emerald-master/ft/metagene/txt/'+str(s)+'-'+str(end[n])
#     txt_dirs.append(txt_dir)
#     predict_dir = '/nfs/production/literature/maaly/emerald-master/ml/ner/biobert/predict/ena/models/tensorflow/ena_all_9010_metagene_'+str(n)
#     predict_dirs.append(predict_dir)
    
# ### PATHS FOR METAGENE ###
# # current directory #     
# n_splt = 7
# txt_dir = txt_dirs[n_splt]
# print(txt_dir)
# predict_dir = predict_dirs[n_splt]
# print(predict_dir)  
# db_sect = 'method'

### GENERATE DATAFRAME FOR PREDICT SENTENCES ###
################################################

#sectdirs = ['all','method']
sectdirs = [db_sect]
datas = []
sentsdfs = []
sentsdfgs = []

for sectdir in sectdirs:
    
    # creating texts dataframe #
    files = Path(txt_dir,sectdir).glob('**/*.txt')
    texts = []
    pmcs = []
    for i, f in enumerate(files):          
        pmc = re.sub(r'.txt','',f.parts[-1])
        text = f.read_text()
        #text = text.encode('ascii', 'ignore').decode("utf-8") # should be commented to keep gene symbols like beta, alpha-tubulin
        if text != '':
            texts.append(text)
            pmcs.append(pmc)   
    data = pd.DataFrame(texts,columns=['text'])
    print(data)
    datas.append(data)

    # tagging sentences words with BIO tags #   
    all_sents={}
    all_sents['PMC'] = []
    all_sents['SENT'] = []     
    for t, txt in enumerate(data['text']):   
        #sents = nltk.sent_tokenize(txt)
        doc = nlp(txt)    
        sents = [sent.text for sent in doc.sents]
        for sent in sents:
            sent = str(sent).strip()                      
            encoded_sent = sent.encode('ascii', 'ignore').decode("utf-8") 
            if encoded_sent != '':
                ### for cpu ###
                all_sents['SENT'].append(sent)
                all_sents['PMC'].append(pmcs[t])                
                ### for gpu ###    
#                 words = bert_tokenize(sent)#bert-tokenize            
#                 #words = nltk.word_tokenize(sent) #nltk-tokenize
#                 #words = [word.text for word in nlp(sent)]
#                 if words==[] or (len(words)==1 and words[0]==''):
#                     print('### UNTOKENIZED:'+str(sent)+' ###')
#                     print(words)
#                 else:    
#                     all_sents['SENT'].append(sent)
#                     all_sents['PMC'].append(pmcs[t])#   

    ### generating sentences dataframes ###
    sentsdf = pd.DataFrame(all_sents)    
    #sentsdf['SENTBB'] = [' '.join(bert_tokenize(snt)) for snt in sentsdf['SENT']]#bert-tokenize
    #sentsdf['SENTBB'] = [' '.join(nltk.word_tokenize(snt)) for snt in sentsdf['SENT']]#nltk-tokenize
    #sentsdf['SENTBB'] = [' '.join([word.text for word in nlp(snt)]) for snt in sentsdf['SENT']]#nltk-tokenize     
    sentsdfs.append(sentsdf)
    sentsdfg = sentsdf.groupby(['PMC'],sort=False)['SENT'].apply(list).reset_index() 
    
### GENERATE ANNOTATIONS IN EPMC FORMAT ###
###########################################
hdf = pd.read_csv(Path(predict_dir,'biobert-grid-metrics.csv')) # high f-scores models

pdfs = {} # predicted entities data frames
dcts = []
for c,catg in enumerate(hdf['category']):
    lr = '3e-05' if hdf['learning_rate'][c] == 2.9999999999999997e-05 else str(hdf['learning_rate'][c])
    mdl = 'html-'+hdf['dataset'][0]+'-ena-'+str(int(hdf['dataset_split'][c]))+'-'+lr+'-'+str(int(hdf['epoch'][c]))
    converters = {}
    for col in ["TERM","TERM-POS","BIO","URL","SENTBB"]:converters[col] = literal_eval
    pdf = pd.read_csv(Path(predict_dir,mdl,'NER_result_'+catg+'.csv'),converters=converters)
    #print(pdf)
    pdfs['SENTBB'] = pdf['SENTBB'] if 'SENTBB'not in pdfs.keys() else pdfs['SENTBB']
    pdfs[catg+'_TERM'] = pdf['TERM']
    pdfs[catg+'_TERM-POS'] = pdf['TERM-POS']
    pdfs[catg+'_BIO'] = pdf['BIO']
    pdfs[catg+'_URL'] = pdf['URL']
    pdfs[catg+'_HTML'] = pdf['HTML']   
    print(mdl)
pdfs = pd.DataFrame(pdfs) 
pdfs['SENT'] = sentsdf['SENT']
pdfs['PMC'] = sentsdf['PMC']
pdfs


pdfsg = pdfs.groupby(['PMC'],sort=False)['SENT'].apply(list).reset_index()
for catg in hdf['category']:
    pdfsg1 = pdfs.groupby(['PMC'],sort=False)[catg+'_TERM'].apply(list).reset_index()
    pdfsg2 = pdfs.groupby(['PMC'],sort=False)[catg+'_BIO'].apply(list).reset_index()
    pdfsg3 = pdfs.groupby(['PMC'],sort=False)[catg+'_URL'].apply(list).reset_index()
    pdfsg4 = pdfs.groupby(['PMC'],sort=False)[catg+'_HTML'].apply(list).reset_index()
    pdfsg5 = pdfs.groupby(['PMC'],sort=False)[catg+'_TERM-POS'].apply(list).reset_index()    
    pdfsg = pdfsg.join(pdfsg1[catg+'_TERM']).join(pdfsg2[catg+'_BIO']).join(pdfsg3[catg+'_URL']).join(pdfsg4[catg+'_HTML']).join(pdfsg5[catg+'_TERM-POS'])
pdfsg6 = pdfs.groupby(['PMC'],sort=False)['SENTBB'].apply(list).reset_index()
pdfsg = pdfsg.join(pdfsg6['SENTBB'])



### EPMC SENTENCE ANNOTATION ###
################################
#jsondir = 'json'
jsondir = 'json-pp'
if os.path.isdir(Path(predict_dir,jsondir)):
    shutil.rmtree(Path(predict_dir,jsondir))
os.mkdir(Path(predict_dir,jsondir),0o775)

xtrms = ['whole','abbr.','abbr','user','users','SI','`-end','-end','total','slide','slides','did not'] #blacklist terms
symbs = [")","(","≥","≤",">","<","~","/",".","-","%","=","+",":","%","°C","ºC","–","±","_","[","]","″","′","’","'","‐"]
symbs2 = ["≤","≥",">","<","~","/",".","-","=","±",":","–","_","″","′","’","'","‐"]
stpwrds = nltk.corpus.stopwords.words('english')
xannpmcs = []

ajf = open(Path(predict_dir,jsondir,'emerald_anns.json'), 'a', encoding='utf-8')

for p,pmcid in enumerate(pdfsg['PMC']):
    print(pmcid)
    mtgn = {} # epmc annotation schema
    mtgn['src'] = 'PMC'
    mtgn['id'] = pmcid      
    mtgn['provider'] = 'emerald'    
    mtgn['anns'] = []
    for catg in hdf['category']:
        for s,snt in enumerate(pdfsg['SENT'][p]):
            stks = pdfsg['SENTBB'][p][s] # sentence tokens
            strms = pdfsg[catg+'_TERM'][p][s] # sentence annotated terms
            spos = pdfsg[catg+'_TERM-POS'][p][s] # sentence annotated terms numbers
            sbios = pdfsg[catg+'_BIO'][p][s] # sentence annotated terms bio 
            surls = pdfsg[catg+'_URL'][p][s] # sentence annotated terms url
            strms2 = [] # sentence annotated terms corrected 
            surls2 = [] # sentence urls corrected
            prfxs = []
            sfxs = []
            if strms !=[]:
                
                sanns = {} # sentence epmc annotations schema
                sanns['exact'] = snt
                sanns['section'] = 'Methods'
                sanns['type'] = catg
                sanns['tags']=[]
                for st,strm in enumerate(strms):                    
                    tbio = sbios[st].split() #term bio                      
                    turl = surls[st] # term url                                        
                    tpos = spos[st] # term number                   
                    ttks = [] # term tokens
                    ttks = strm.split(' ') # term tokens                       
                    if len(ttks)>1:
                        tpos2 = [tpos-tk for tk,ttk in enumerate(ttks)]
                        tpos2.sort()
                    else:
                        tpos2 = [tpos] # term corrected numbers
                    ttks2 = [stks[ps-1] for ps in tpos2] # term tokens corrected
                    #print(ttks2)
                                        
                    ### Getting missing B-MISC terms - comment this section if you don't want to get the missing terms ###
                    ######################################################################################################
                    try:
                        if (tbio[0] == 'I-MISC') or (ttks2[0] in symbs2):
                            if tpos2[0]>1: 
                                tpos2 = [*[tpos2[0]-1],*tpos2]                                
                                ttks2 = [stks[ps-1] for ps in tpos2]
                                ttks3 = ttks2
                                if tpos2[0]>1:ttks3 = [stks[ps-1] for ps in [*[tpos2[0]-1],*tpos2]]
                                while (ttks2[0] in symbs or ttks2[0] in stpwrds or ttks3[0] in symbs) and tpos2[0]>1:
                                    tpos2 = [*[tpos2[0]-1],*tpos2]                                
                                    ttks2 = [stks[ps-1] for ps in tpos2]
                                    ttks3 = ttks2
                                    if tpos2[0]>1:ttks3 = [stks[ps-1] for ps in [*[tpos2[0]-1],*tpos2]]                                                

                        if tpos2[-1]+1<len(stks):ttks3 = [stks[ps-1] for ps in [*tpos2,*[tpos2[-1]+1]]]
                        if tpos2[-1]<len(stks) and (ttks2[-1] in symbs2 or ttks2[-1] in stpwrds or ttks3[-1] in symbs2):
                            tpos2 = [*tpos2,*[tpos2[-1]+1]]
                            ttks2 = [stks[ps-1] for ps in tpos2]
                            ttks3 = ttks2
                            if tpos2[-1]+1<len(stks):ttks3 = [stks[ps-1] for ps in [*tpos2,*[tpos2[-1]+1]]]                               
                            while tpos2[-1]<len(stks) and ttks2[-1] in symbs2:                                
                                tpos2 = [*tpos2,*[tpos2[-1]+1]]
                                ttks2 = [stks[ps-1] for ps in tpos2]
                                ttks3 = ttks2
                                if tpos2[-1]+1<len(stks):ttks3 = [stks[ps-1] for ps in [*tpos2,*[tpos2[-1]+1]]]
                                    
                        while ttks2[0] in [',',')',']',';'] or (ttks2[0] in stpwrds and ttks[0] not in stpwrds) or (ttks2[0] in symbs and ttks[0] not in symbs): ttks2 = ttks2[1:]
                        while ttks2[-1] in [',','(','[',';'] or (ttks2[-1] in stpwrds and ttks[-1] not in stpwrds) or (ttks2[-1] in symbs and ttks[-1] not in symbs): ttks2 = ttks2[:-1]
                    
                    except:                        
                        print('##########')
                        print(snt)
                        print(strm)
                        print(ttks2)                        
                        print('##########')
                    
                    ### without getting missing B-MISC terms - start of main script ###
                    ###################################################################
                    trms = []
                    rgxs = ['\s?','.{0,1}','.{0,2}','.{0,3}','.{0,4}','.{0,5}','.{0,6}','.{0,7}'] # regular expressions set                        
                    for rgx in rgxs:                            
                        strm2 = ' '.join(ttks2)
                        strm2 = re.escape(strm2)                                
                        strm2 = strm2.replace('\ ',rgx)                                 
                        trms = re.findall(strm2,snt)
                        trmspos = [p.start() for p in re.finditer(strm2,snt)]                           
                        if trms != []:                            
                            break
                        else:
                            print('#####')
                            print(snt)
                            print(strm2)
                            print(rgx)
                            print(trms)
                            print('#####')
                    
                    # processing primer tokenizing error #
                    if catg == 'primer' and trms == []:                    
                        for rgx in rgxs:                            
                            strm2 = ' '.join(ttks2)                            
                            if not re.search(r'.+[5\′]',strm2) is None:
                                res = re.search(r'.+[5\′]|',strm2)
                                strm2 = res.group()
                                strm2 = strm2.replace('5 ′','')
                                strm2 = re.escape(strm2)                                
                                strm2 = strm2.replace('\ ',rgx)                                 
                                trms = re.findall(strm2,snt)
                                trmspos = [p.start() for p in re.finditer(strm2,snt)]                           
                                if trms != []:                            
                                    break
                                else:
                                    print('#####')
                                    print(snt)
                                    print(strm2)
                                    print(rgx)
                                    print(trms)
                                    print('#####')                 
                    
                    
                    # remove common false positives terms #                    
                    if catg not in ['date','place','primer','gene']:
                        trms = [trm for trm in trms if re.search(r'^[0-9\%\≤\≥\>\<\~\/\.\-\=\±\:\−\–\_\″\′\’\'\"\]\[\)\(\‐\$]+$',trm) is None]
                    #if catg in ['anthropogenic','site']:
                        #trms = [trm for trm in trms if re.search(r'^[A-Z0-9\.]$',trm.strip()) is None]
                    if catg in ['place','site']:
                        trms = [trm for trm in trms if re.search(r'^[A-Z]{1}\.\s?[a-z]+$',trm.strip()) is None]    
                    trms = [trm for trm in trms if trm.lower() not in xtrms and re.search(r'^[0-9\.\,]{0,7}\s?[a-zμ]{1,2}\/[a-zμ]{1,2}$',trm.lower().strip()) is None]
                    # getting term context #
                    #print(rgx)
                    cpos = tpos2

                    if len(list(np.unique(trms)))==1:
                        ctxt = re.search(strm2,snt)
                        cpos = tpos2
                    else:                                           
                        if tpos2[0]-2>0 and tpos2[-1]+2<=len(stks):
                            cpos = [*[tpos2[0]-2],*[tpos2[0]-1],*tpos2,*[tpos2[-1]+1],*[tpos2[-1]+2]]       
                        elif tpos2[0]-1>0 and tpos2[-1]+1<=len(stks):
                            cpos = [*[tpos2[0]-1],*tpos2,*[tpos2[-1]+1]]
                        elif tpos2[0]==1 and tpos2[-1]+2<=len(stks):
                            cpos = [*tpos2,*[tpos2[-1]+1],*[tpos2[-1]+2]]
                        elif tpos2[0]==1 and tpos2[-1]+1<=len(stks):
                            cpos = [*tpos2,*[tpos2[-1]+1]]
                        elif tpos2[-1]==len(stks) and tpos2[0]-1>0:
                            cpos = [*[tpos2[0]-1],*tpos2]        
                        elif tpos2[-1]==len(stks) and tpos2[0]-2>0:
                            cpos = [*[tpos2[0]-2],*[tpos2[0]-1],*tpos2]                        
                    
                    for rgx in rgxs:
                        ctxt = ' '.join([stks[ps-1] for ps in cpos])
                        ctxt = re.escape(ctxt)
                        ctxt = ctxt.replace('\ ',rgx)               
                        ctxt = re.search(ctxt,snt)
                        if not ctxt is None:
                            break;
                    
                    if ctxt is None:
                        print('#######')
                        print(stks)
                        print(strm)
                        print(tpos2)
                        print(ttks2)
                        print(strm2)                 
                        print('#######')
                    else:
                        cpos = ctxt.span()
                    
                    # adding terms #                       
                    if trms!=[]:                               
                        for t,trm in enumerate(trms):                         
                            if trmspos[t] in range(cpos[0],cpos[1]) and trm not in [*strms2,*symbs,*stpwrds]:
                                strms2.append(trm)                                    
                                surls2.append(turl)
                                prfxs.append(snt[0:trmspos[t]])
                                sfxs.append(snt[trmspos[t]+len(trm):])
                    
            
            ### adding entities ###
            if strms2 !=[]:                   
                if len(np.unique(strms2))!=len(np.unique(strms)):
                    print(snt)
                    print(stks)
                    print(sbios)
                    print(spos)
                    print(strms)
                    print(strms2)                       
                for t,trm in enumerate(strms2):
                    trmo={}
                    trmo['name']=trm
                    trmo['prefix']=prfxs[t]
                    trmo['postfix']=sfxs[t]
                    trmo['uri']=surls2[t] if surls2[t]!='' else 'https://www.ebi.ac.uk/ols/search?q='+trm
                    sanns['tags'].append(trmo)               
                mtgn['anns'].append(sanns)
                
    if mtgn['anns']!=[]:        
        ### doesn't work with EPMC validator ###        
        with open(Path(predict_dir,jsondir,pmcid+'.json'), 'w', encoding='utf-8') as j:
            json.dump(mtgn,j,ensure_ascii=False,indent=4)

        ### works with EPMC validator but better to merge all pmcs annotations into one file (10k lines each) - ajf ###
        #jf =  open(Path(predict_dir,jsondir,pmcid+'.json'), 'w', encoding='utf-8')
        #jf.write(json.dumps(mtgn))
        #jf.close()

        ### appending to all pmc merged file ###
        ajf.write(json.dumps(mtgn,ensure_ascii=False)+'\n')
    else:
        xannpmcs.append(pmcid)                
                        
ajf.close()

### EPMC ENTITY ANNOTATION ###
##############################

jsdir = Path(predict_dir,'json-pp')
### open pmcs json ###
jsfs = list(jsdir.glob('**/*.json'))

odir = Path(predict_dir,'ent-json-pp')
if os.path.isdir(odir):
    shutil.rmtree(odir)
os.mkdir(odir,0o775)

splt = 9999
range1 = list(range(0,len(jsfs),splt)) # compounds starting indices
range2 = [*list(range(splt,len(jsfs),splt)),*[len(jsfs)]] # compounds ending indices  

fsplts = []
for s,strt in enumerate(range1):
    fsplt = jsfs[strt:range2[s]]
    fsplts.append(fsplt)
    print(len(fsplt))
    
for f,fsplt in enumerate(fsplts):
    if db == 'ena':
        n_splt = f
    else:
        f=n_splt
    ajf = open(Path(odir,'emerald_anns_'+db+'_'+str(n_splt)+'_'+str(len(fsplt))+'_M2.json'), 'a', encoding='utf-8')
    xpfxs = []
    for j,jsf in enumerate(fsplt):
        jtyps = []
        jf = jsf.parts[-1] 
        if jf != 'emerald_anns.json':
            print(j)
            print(jf)
            with open(jsf) as ijf:
                js = json.load(ijf)           
            ejs = {}
            ejs['src'] = js['src']
            ejs['id'] = js['id']
            ejs['provider'] = 'Metagenomics'
            ejs['anns'] = []        
            for ann in js['anns']:            
                snt = ann['exact']
                for tag in ann['tags']:
                    ent = {}
                    ent['exact'] = tag['name']
                    ent['position'] = '1.0'                               
                    ent['prefix'] = tag['prefix']
                    ent['postfix'] = tag['postfix']
                    ent['section'] = ann['section']
                    typ = ann['type']
                    if typ == 'anatomy': typ = 'body-site'
                    if typ == 'anthropogenic': typ = 'engineered'
                    if typ == 'material': typ = 'sample-material' 
                    ent['type'] = typ # lower case - types should be lower case 
                    #ent['type'] = typ.title() # upper case first letter
                    #print(typ.title())
                    jtyps.append(typ)
                    ent['tags'] = [{'name':tag['name'],'uri':tag['uri']}]
                    #print(ent)                
                    if (tag['prefix'] =='' and tag['postfix'] =='') or (tag['prefix'] is None and tag['postfix'] is None) or tag['name']=='': 
                        xpfxs.append([js['id'],tag])                    
                    else:
                        ejs['anns'].append(ent)                    
            #print(ejs)
            if len(jtyps)<4:
                print(Counter(jtyps))
                print(js['id']) 
                xjs.append(js['id'])
            if len(jtyps)>=4:    
                with open(Path(odir,jf), 'w', encoding='utf-8') as ojf:
                    json.dump(ejs,ojf,ensure_ascii=False,indent=4) #indent format the json

            ### appending to all pmc merged file ###
            #if jf in ['PMC5112336.json','PMC7381539.json','PMC5013295.json','PMC6580296.json','PMC7095418.json','PMC5131798.json','PMC4544774.json','PMC6544527.json','PMC6736778.json']:
            ajf.write(json.dumps(ejs,ensure_ascii=False)+'\n')
    ajf.close()    